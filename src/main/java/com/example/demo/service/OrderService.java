package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Order;
import com.example.demo.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository repository;

	public List<Order> getAllOrders() {
		return repository.getAllOrders();
	}

	public Order getOrder(int id) {
		return repository.getOrderById(id);
	}

	public Order saveOrder(Order order) {
		return repository.editOrder(order);
	}

	public Order newOrder(Order order) {
		return repository.addOrder(order);
	}

	public int deleteOrder(int id) {
		return repository.deleteOrder(id);
	}
}
