package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Order;
import com.example.demo.service.OrderService;

@RestController
@RequestMapping("api/orders")
public class OrderController {
	@Autowired
	OrderService service;

	@GetMapping(value = "/")
	public List<Order> getAllOrders() {
		// System.out.println("I am here");
		return service.getAllOrders();
	}

	@GetMapping(value = "/{id}")
	public Order getOrderById(@PathVariable("id") int id) {
		return service.getOrder(id);
	}

	@PostMapping(value = "/")
	public Order addOrder(@RequestBody Order order) {
		return service.newOrder(order);
	}

	@PutMapping(value = "/")
	public Order editOrder(@RequestBody Order order) {
		return service.saveOrder(order);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteOrder(@PathVariable int id) {
		return service.deleteOrder(id);
	}
}
