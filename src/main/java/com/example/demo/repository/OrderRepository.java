package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Order;

@Component
public interface OrderRepository {
	public List<Order> getAllOrders();

	public Order getOrderById(int id);

	public Order editOrder(Order order);

	public int deleteOrder(int id);

	public Order addOrder(Order order);
}
