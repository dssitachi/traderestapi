package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Order;

@Repository
public class MySQLOrderRepository implements OrderRepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<Order> getAllOrders() {
		// TODO Auto-generated method stub
		String sql = "SELECT * from Orders";
		return template.query(sql, new OrderRowMapper());
		// String sql = "SELECT OrderID, CompanyName, Phone FROM Orders";
		// return template.query(sql, new OrderRowMapper());
	}

	@Override
	public Order getOrderById(int id) {
		// TODO Auto-generated method stub
		// String sql = "SELECT * FROM Shippers WHERE id=?";
		// return template.queryForObject(sql, new OrderRowMapper(), id);
		String sql = "SELECT * FROM Orders WHERE id=?";
		return template.queryForObject(sql, new OrderRowMapper(), id);
	}

	@Override
	public Order editOrder(Order order) {
		// TODO Auto-generated method stub
		String sql = "UPDATE Orders SET userId = ?, stockTicker = ?, price = ?, volume = ?, buyOrSell = ?, statusCode = ?, currentTime = ? "
				+ "WHERE id=?";
		template.update(sql, order.getUserId(), order.getStockTicker(), order.getPrice(), order.getVolume(),
				order.getBuyOrSell(), order.getStatusCode(), order.getCurrentTime(), order.getId());
		return order;
	}

	@Override
	public int deleteOrder(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM Orders WHERE id=?";
		template.update(sql, id);
		return id;
	}

	@Override
	public Order addOrder(Order order) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO Orders(userId,stockTicker,price,volume,buyOrSell,statusCode,currentTime) "
				+ "VALUES(?,?,?,?,?,?,?)";
		template.update(sql, order.getUserId(), order.getStockTicker(), order.getPrice(), order.getVolume(),
				order.getBuyOrSell(), order.getStatusCode(), Timestamp.from(Instant.now()));
		return order;
	}
}

class OrderRowMapper implements RowMapper<Order> {

	@Override
	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Order(rs.getInt("id"), rs.getInt("userId"), rs.getString("stockTicker"), rs.getDouble("price"),
				rs.getInt("volume"), rs.getString("buyOrSell"), rs.getInt("statusCode"),
				rs.getTimestamp("currentTime"));

	}

}
