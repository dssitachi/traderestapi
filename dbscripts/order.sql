use trade;

CREATE Table Users(
userId int primary key auto_increment Not NULL,
userName varchar(50) not null,
userEmail varchar(80),
userMobile varchar(200) not null,
userAddress varchar(100) not null,
userDob  date,
gender varchar(15)
);

create table Wallet(
userId int,
walletBalance int not null,
accountNumber int not null,
panCard varchar(10) not null,
FOREIGN KEY (userId) REFERENCES Users(userId),
portfolioValue int
);

create Table Orders(
orderId int primary key auto_increment Not NULL,
userId int,
 stockTicker varchar(10) not null,
	price double not null ,
	volume int not null,
	buyOrSell varchar(4) not null,
	statusCode int not null,
	currentTime varchar(30) not null,
    FOREIGN KEY (userId) REFERENCES Users(userId)
);

use trade;
INSERT into Users(
userName,
userEmail,
userMobile ,
userAddress,
userDob,
gender
)
values
('user1','user1@gmail.com','1111111111','add1','1999-01-01','F'),
('user2','user2@gmail.com','2222222222','add2','1999-02-02','M');

INSERT into Orders (
	userId,
	stockTicker,
    price,
    volume,
    buyOrSell,
    statusCode,
    currentTime
    
)
values
( 1,'IEC',15.27,50,'buy', 20,now()),
(1,'XONE',25.08,80,'buy',0,now()),
(1,'KNBE',26.48,100,'buy',1,now()),
(1,'SESN',4.91,100,'buy',2,now()),
(2,'LFST',11.71,90,'sell',0,now()),
(2,'GOCO',4.69,200,'sell',1,now()),
(2,'TIG',9.38,45,'sell',0,now()),
(2,'CSSE',22.22,49,'sell',2,now());


INSERT into Wallet(
userId,
walletBalance,
accountNumber,
panCard,
portfolioValue)
values
(1,1000,1234,'pan1',1),
(2,2000,2345,'pan2',2);

select * from orders;

Insert into Orders(
userId,
	stockTicker,
    price,
    volume,
    buyOrSell,
    statusCode,
    currentTime
) values(1,"REL",10.01,20,"buy",1,"");

select * from Orders;
select * from Wallet;

